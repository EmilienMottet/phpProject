<?=VueHtmlUtils::enTeteHTML5('Bienvenue sur notre site', 'UTF-8','css/defaultStyle.css');?>
<?=VueHtmlUtils::menu();?>
    <h1>Connexion</h1>

<?php
    echo (isset($model->getError()['login']) ? "<p>".$model->getError()["login"]."<br/>" : "");
    echo VueHtmlUtils::getHTML_LoginForm(Config::getRootURI());
?>

<?=VueHtmlUtils::finFichierHtml();?>