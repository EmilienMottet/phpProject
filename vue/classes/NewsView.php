<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 13/12/2015
 * Time: 23:35
 */
class NewsView
{
    public static function getHtmlDeveloped($news){
        $htmlCode = "";
        $htmlCode .= "<strong>Auteur : </strong>\n";
        $htmlCode .= $news->getAuteur()."<br/>";
        if(!empty($news->getAuteur()))
            $htmlCode .= "News : <br/>";
        $htmlCode .= $news->getText();
        if(!empty($news->getText()))
            $htmlCode .= "<br/>";

        return $htmlCode;
    }

    public static function getHtmlCompact($news){
        $htmlCode = "";
        $htmlCode .="<strong> Auteur : </strong>\n";
        $htmlCode .= $news->getAuteur()."<br/>";
        if(!empty($news->getAuteur()))
            $htmlCode .= "News : <br/>";
        $htmlCode .= $news->getText();
        if(!empty($news->getText()))
            $htmlCode .= "<br/>";
        $htmlCode .= "<a href='".Config::getRootURI()."?action=afficheNews&id=".$news->getId()."'>lire la suite</a>"; // a modif le lien
        return $htmlCode;
    }

    public static function getHtmlCommentaire($news){
        $htmlCode = "";
        foreach ($news->getCommentaires() as $commentaire) {
            $htmlCode .= CommentaireView::getHtmlDeveloped($commentaire);
        }

        return $htmlCode;

    }
}

?>