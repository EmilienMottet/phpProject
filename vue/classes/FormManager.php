<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 13/12/2015
 * Time: 13:45
 */
class FormManager
{
    /**
    @brief gÃ©nÃ¨re la balise form avec sa mÃ©thode (Post ou Get) et sont action (url)
     */
    public static function beginForm($method, $action, $css_class="", $extraOptions="") {
        $css_class_option = "";
        if (!empty($css_class))
            $css_class_option = "class=\"".$css_class."\" ";
        return "<form method=\"".$method."\" action=\"".$action."\" ".$css_class_option.$extraOptions.">\n";
    }

    /**
    @brief ferme le formulaire
     */
    public static function endForm() {
        return "</form>";
    }

    /**
    mÃ©thode gÃ©nÃ©rique de gÃ©nÃ©ration d'un input
    @param $labelText texte du label correspondant Ã  l'input
    @param $type type d'input : texte textarea, chackbox, radio, submit...
    @param $id ID de l'input pour la correspondance label/input
    @param $value valeur initiale du champs de l'input
    @paarm $extraOptions chaine de caractÃ¨res contenant les options supplÃ©mentaires
    de l'input suivant la syntaxe HTML.
     */
    public static function addInput($labelText, $type, $name, $id, $value=null, $extraOptions="", $noBR=false) {
        $returnText ="";
        $valueOption = ($value == null) ? "" : " value=\"".$value."\" ";
        if ($extraOptions == null) {
            $extraOptions="";
        }

        if ($labelText!=null && $labelText!=""){
            $returnText .= "<label for=\"".$id."\">".$labelText."</label>\n";
        }
        $returnText .= "<input type=\"".$type."\" name=\"".$name."\" id=\"".$id."\" ".$valueOption." ".$extraOptions." />\n";

        if (!$noBR){
            $returnText .= "<br/>\n";
        }
        return $returnText;
    }

    /**
    @brief mÃ©thode simplifiÃ©e pour gÃ©nÃ©rer un input de type text
     */
    public static function addTextInput($labelText, $name, $id, $size, $value=null, $extraOptions="") {
        return self::addInput($labelText, "text", $name, $id, $value, "size =\"".$size."\" ".$extraOptions);
    }

    /**
    @brief mÃ©thode simplifiÃ©e pour gÃ©nÃ©rer un input de type password
     */
    public static function addPasswordInput($labelText, $name, $id, $size, $value=null, $extraOptions="") {
        return self::addInput($labelText, "password", $name, $id, $value, "size =\"".$size."\" ".$extraOptions);
    }

    /**
    @brief mÃ©thode simplifiÃ©e pour gÃ©nÃ©rer un input de type radio
     */
    public static function addRadioInput($labelText, $name, $id, $checked, $value=null, $extraOptions="") {
        return self::addInput($labelText, "radio", $name, $id, $value, (strcmp($checked, 'checked')==0)? "checked =\"checked\" ":" ".$extraOptions);
    }

    /**
    @brief mÃ©thode simplifiÃ©e pour gÃ©nÃ©rer un input de type radio
     */
    public static function addCheckboxInput($labelText, $name, $id, $checked, $value, $extraOptions="") {
        return self::addInput($labelText, "checkbox", $name, $id, $value, (strcmp($checked, 'checked')==0)? "checked =\"checked\" ":" ".$extraOptions);
    }

    /**
    @brief mÃ©thode simplifiÃ©e pour gÃ©nÃ©rer un input de type textarea
     */
    public static function addTextAreaInput($labelText, $name, $id, $rows, $cols, $value=null, $extraOptions="") {
        $returnText ="";
        $valueOption = ($value == null) ? "" : $value;
        if ($extraOptions == null) {
            $extraOptions="";
        }
        $returnText .= "<p>\n";
        if ($labelText!=null  && $labelText!=""){
            $returnText .= "<label for=\"".$id."\">".$labelText."</label>\n";
        }
        $returnText .= "<textarea name=\"".$name."\" id=\"".$id."\" rows=\"".$rows."\" cols=\"".$cols."\" ".$extraOptions." >".$valueOption."</textarea>\n";
        $returnText .= "</p>\n";
        return $returnText;
    }

    /**
    @brief mÃ©thode simplifiÃ©e pour gÃ©nÃ©rer un input de type file (upload)
     */
    public static function addUploadInput($labelText, $name, $id, $size, $value="", $extraOptions="") {
        $valueOption = ($value == null) ? "value=\"\"" : " value=\"".$value."\" ";
        if ($extraOptions == null) {
            $extraOptions="";
        }
        return self::addInput($labelText, "file", $name, $id, $value, "size =\"".$size."\" ".$valueOption." ".$extraOptions);
    }

    /**
    @brief mÃ©thode  pour commencer un select
     */
    public static function beginSelect($labelText, $name, $id, $multiple=false, $size=6) {
        $returnText = "";
        if ($multiple)
            $multipleOption="multiple=\"multiple\" size=\"".$size."\"";
        else
            $multipleOption="";
        if ($labelText!=null  && $labelText!=""){
            $returnText .= "<p><label for=\"".$id."\">".$labelText."</label>\n";
        }
        $returnText .= "<select name=\"".$name."\" id=\"".$id."\"".$multipleOption.">\n";
        return $returnText;
    }

    /**
    @brief mÃ©thode simplifiÃ©e pour terminer un select
     */
    public static function endSelect(){
        $returnText = "</select></p>\n";
        return $returnText;
    }

    /**
    @brief mÃ©thode simplifiÃ©e pour ajouter une option de select
     */
    public static function addSelectOption($value,$displayText, $selected=false){
        $returnText = "";
        if ($selected)
            $selectedOption="selected=\"selected\"";
        else
            $selectedOption="";
        $returnText .= "<option value=\"".$value."\" ".$selectedOption.">".$displayText."</option>\n";
        return $returnText;
    }

    /**
    @brief mÃ©thode simplifiÃ©e pour gÃ©nÃ©rer un input de type radio
     */
    public static function addHiddenInput($name, $id, $value, $extraOptions="") {
        return self::addInput("", "hidden", $name, $id, "".$value, $extraOptions, true);
    }

    /**
    @brief mÃ©thode simplifiÃ©e pour gÃ©nÃ©rer un bouton submit
     */
    public static function addSubmitButton($value="Envoyer", $extraOptions="") {
        return self::addInput(null, "submit", "", "", $value, " ".$extraOptions);
    }
}

?>