<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 13/12/2015
 * Time: 23:35
 */
class CommentaireView
{
    public static function getHtmlDeveloped($commentaire){
        $htmlCode ="";
        $htmlCode .="<strong> Commentaire de : </strong>\n";
        $htmlCode .= $commentaire->getAuteur()."<br/>" ;
//        if(!empty($commentaire->getAuteur()))
//            $htmlCode .= "Commentaire : <br/>";
        $htmlCode .= $commentaire->getText();
        if(!empty($commentaire->getText()))
            $htmlCode .= "<br/>";

        return $htmlCode;
    }

    //useless
    public static function getHtmlCompact($Commentaire){
        $htmlCode = "";
        $htmlCode .="<strong> Auteur : </strong>\n";
        $htmlCode .= $Commentaire->getAuteur()."<br/>";
        if(!empty($Commentaire->getAuteur()))
            $htmlCode .= "Commentaire : <br/>";
        $htmlCode .= $Commentaire->getText();
        if(!empty($Commentaire->getText()))
            $htmlCode .= "<br/>";
        $htmlCode .= "<a href='".Config::getRootURI()."?action=afficheCommentaire&id=".$Commentaire->getId()."'>lire la suite</a>"; // a modif le lien
        return $htmlCode;
    }
}

?>