<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 22/12/2015
 * Time: 00:41
 */
class AuthUtils
{

//    /** Fonction qui teste si un mot de passe est suffisemment difficile */
//    public static function isStrongPassword($wouldBePasswd){
//        $lengthCondition = (strlen($wouldBePasswd) >= 8 && strlen($wouldBePasswd) <= 35);
//        // On peut sûrement faire plus efficace pour l'évaluation des expressions régulières...
//        $CharacterDiversityCondition = preg_match("/[a-z]/", $wouldBePasswd)
//            && preg_match("/[A-Z]/", $wouldBePasswd)
//            && preg_match("/[0-9]/", $wouldBePasswd)
//            && preg_match("/[\#\-\|\.\@\[\]\=\!\&]/", $wouldBePasswd);
//        return $lengthCondition && $CharacterDiversityCondition;
//    }
//
//    // fonction de génération de l'ID de session avec
//    // partie aléatoire et partie contrôle par adresse IP
//    public static function generateSessionIdAndCookie($ipAddress, &$mySid_part1){
//        // Génération de 5 octets (pseudo-)aléatoires codés en hexa
//        $cryptoStrong = false; // Variable pour passage par référence
//        $octets = openssl_random_pseudo_bytes(5, $cryptoStrong);
//        $mySid_part1 = bin2hex($octets);
//
//        // On applique un hash sur l'adresse IP pour éviter l'usurpation d'identité
//        // Un pirate aura besoin de temps pour deviner l'IP et l'usurper,
//        // en devinant aussi la partie aléatoire du SID, surtout avec une connexion SSL...
//        $mySid_part2 = hash("md5" , $ipAddress);
//        // Le numéro de session contient l'information de l'adresse IP
//        // Quelqu'un avec une autre adresse IP ne pourra pas accéder à cette session
//        // suivant notre implémentation (sauf à générer une colision sur md5 (Hahaha !))
//        $mySid = $mySid_part1.$mySid_part2;
//        return $mySid;
//    }
//
//    // inutile si j'ai bien compris
//    // Fonction à implémenter : test d'existance du login/mot de passe en BD
//    public static function userPasswordCheckInDatabase($email, $hashedPassword){
//        // TODO : tester si le couple e-mail et mot de passe (après hashage SHA512)
//        // sont bien présents dans la base de données
//        return true;
//    }
//

    public static function isStrongPassword($wouldBePasswd){
        $lengthCondition = (strlen($wouldBePasswd) >= 8 && strlen($wouldBePasswd) <= 35);
        $CharacterDiversityCondition = preg_match("/[a-z]/",$wouldBePasswd) && preg_match("/[A-Z]/",$wouldBePasswd) && preg_match("/[0-9]/",$wouldBePasswd);
        return $lengthCondition && $CharacterDiversityCondition;
    }

    public static function generateSessionId(){
        $cryptoStrong = false;
        $octets = openssl_random_pseudo_bytes(10,$cryptoStrong);
        $mySid = bin2hex($octets);
        return $mySid;
    }

}