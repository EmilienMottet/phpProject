<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 03/12/2015
 * Time: 11:29
 */
class VueHtmlUtils
{
    public static function enTeteHTML5($title, $charset, $css_sheet){
        // sortie du doctype. Les guillemets HTML sont protégés par \
        echo "<!doctype html>\n";
        echo "<html lang=\"fr\">\n";
        echo "<head>\n";
        echo "<meta charset=\"";
        echo $charset;
        echo "\"/>\n";
        echo "<link rel=\"stylesheet\" href=\"";
        echo $css_sheet;
        echo "\" />\n";
        // concaténation de chaînes de caractères.
        echo "<title>".$title."</title>\n";
        echo "</head>\n<body id='main'>\n";
    }

    public static  function finFichierHtml()
    {
        echo "</body>\n</html>\n";
    }

    public static function menu(){
        echo '<section id="top-nav">
						<nav>
									<a href="'.Config::getRootURI().'" id="logo">
												<img src="http://www.clker.com/cliparts/c/1/a/1/12985810301278174831dave_film.png">
												<div>cine new</div>
									</a>
									<div id="menu">
												<a href="'.Config::getRootURI().'?action=afficheCollectionNews" class="menu-link current-page">
					News
				</a>
												<a href="#film" class="menu-link">
					Film
				</a>
												</div>
									</div>
									<div id="search">
												<form>
															<select>
																		<option>News</option>
																		<option>Film</option>
															</select>
															<input type="text" placeholder="search">
															<input type="submit" value="go">
												</form>
									</div>
									<div id="account">
												<a href="'.Config::getRootURI().'?action=auth">Log in</a> or
												<a href="'.Config::getRootURI().'?action=inscription">Create Account</a>
									</div>
						</nav>
			</section>';
    }

    public static function getHTML_LoginForm($formAction){
        $htmlCode ="";

        if( !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
            $htmlCode .= "<p><strong>Warning : </strong>Vous n'êtes pas sur une connexion sécurisée<i>HTPPS</i> avec <i>SSL</i> . <br/> Votre confidentialité n'est pas garantie ! </p>";
        }

        $htmlCode .= '<form method="POST" action="'.$formAction.'">';
        $htmlCode .= '<input type="hidden" name="action" value="validateAuth" />';
        $htmlCode .= '<p><label for="email">e-mail</label> <input type="email" name="email" size="25"/></p>';
        $htmlCode .= '<p><label for="motdepasse">Mot de passe</label><input type="password" name="motdepasse" size="25"/></p>';
        $htmlCode .= '<input class="sansLabel" value="Envoyer" type="submit"/>';
        $htmlCode .= '</form>';
        $htmlCode .= "<p>L'adresse <i>e-mail</i> doit etre valide et votre mot de passe doit contenir au moins 8 caractères, une minuscule, une majuscule, et un chiffre</p>";
        return $htmlCode;
    }

    public  static function getHTML_InscriptionForm($formAction){
        $htmlCode ="";

        if( !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
            $htmlCode .= "<p><strong>Warning : </strong>Vous n'êtes pas sur une connexion sécurisée<i>HTPPS</i> avec <i>SSL</i> . <br/> Votre confidentialité n'est pas garantie ! </p>";
        }

        $htmlCode .= '<form method="POST" action="'.$formAction.'">';
        $htmlCode .= '<input type="hidden" name="action" value="validateInsc" />';
        $htmlCode .= '<p><label for="email">e-mail</label> <input type="email" name="email" size="25"/></p>';
        $htmlCode .= '<p><label for="motdepasse">Mot de passe</label><input type="password" name="motdepasse" size="25"/></p>';
        $htmlCode .= '<p><label for="motdepasseComfirmation">Mot de passe ( Comfirmation )</label><input type="password" name="motdepasseComfirmation" size="25"/></p>';
        $htmlCode .= '<input class="sansLabel" value="Envoyer" type="submit"/>';
        $htmlCode .= '</form>';
        $htmlCode .= "<p>L'adresse <i>e-mail</i> doit etre valide et votre mot de passe doit contenir au moins 8 caractères, une minuscule, une majuscule, et un chiffre</p>";
        return $htmlCode;
        return $htmlCode;
    }

}

?>