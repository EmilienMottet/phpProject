<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 13/12/2015
 * Time: 22:48
 */
class NewsFormView
{
    public static function getFormHtml($action, $news){
        $htmlCode = FormManager::beginForm("post",$action);
        $htmlCode .= FormManager::addTextInput("Auteur","auteur","auteur","4",html_entity_decode($news->getAuteur(),ENT_QUOTES,"UTF-8"))."<br/>";

        $htmlCode .= FormManager::addTextAreaInput("Text","text","text","30","60",html_entity_decode($news->getText(),ENT_QUOTES,"UTF-8"))."<br/>";

        $htmlCode .= FormManager::addSubmitButton("Poster");

        $htmlCode .= FormManager::endForm();

        return $htmlCode;
    }

    public static  function getDefaultFormHTML($action){
        return self::getFormHtml($action, News::getDefaultNews());
    }

    private static function addErrorMsg($dataErrors, $fieldName){
        $htmlCode = "";
        if (!empty($dataErrors[$fieldName])){
            $htmlCode .= "<span class=\"errorMsg\">".$dataErrors[$fieldName]."</span><br/>";
        }
        return $htmlCode;
    }

    public static function getFormErrorsHtml($action, $news, &$dataErrors){
        $htmlCode = FormManager::beginForm("post",$action);

        $htmlCode .= self::addErrorMsg($dataErrors, "auteur");

        $htmlCode .= FormManager::addTextInput("Auteur","auteur","auteur","4",html_entity_decode($news->getAuteur(),ENT_QUOTES,"UTF-8"))."<br/>";

        $htmlCode .= self::addErrorMsg($dataErrors,"text");

        $htmlCode .= FormManager::addTextAreaInput("Text","text","text","30","60",html_entity_decode($news->getText(),ENT_QUOTES,"UTF-8"))."<br/>";

        $htmlCode .= FormManager::addSubmitButton("Poster");

        $htmlCode .= FormManager::endForm();

        return $htmlCode;
    }

    public static function getHiddenFormHtml($action, $news, $buttonText){
        $htmlCode = FormManager::beginForm("post",$action);
        $htmlCode .= FormManager::addHiddenInput("auteur","auteur",html_entity_decode($news->getAuteur(),ENT_QUOTES,"UTF-8"));
        $htmlCode .=FormManager::addHiddenInput("text","text",html_entity_decode($news->getText(),ENT_QUOTES,"UTF-8"));
        $htmlCode .=FormManager::addSubmitButton($buttonText, "class=\"sansLabel\"");
        $htmlCode .=FormManager::endForm();
        return $htmlCode;
    }


}

?>