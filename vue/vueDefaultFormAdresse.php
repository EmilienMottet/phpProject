<?php
require_once(dirname(__FILE__).'/VueHtmlUtils?php');
require_once(dirname(__FILE__).'/NewsFormView.php');

echo VueHtmlUtils::enTeteHTML5('Saisie d\'une news','UTF-8',"defaultStyle.css");

echo NewsFormView::getDefaultFormHTML('receptionNews');

echo VueHtmlUtils::finFichierHtml();
?>