<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 17/12/2015
 * Time: 11:24
 */
class NewsGateway
{
    public static  function getNewsById(&$dataError, $id){
        $news="";
        if(isset($id)){
            $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('SELECT * FROM tnews WHERE id = ?',array($id));
            if(isset($queryResults) && is_array($queryResults)){
              if(count($queryResults) === 1){
                  $row = $queryResults[0];

                  $news = NewsFabrique::getNewsAvecCommentaires($dataError,$row['id'],$row['auteur'],$row['text'],CommentaireGateway::getCommentaireAllByNews($dataError,$row['id']));
              }else{
                  $dataError['persistance'] = "News introuvable";
              }
            }else{
                $dataError['persistance'] = "Impossible d'accéder aux données.";
            }
        }
        return $news;
    }

    public  static  function getNewsAll(&$dataError){
        $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('SELECt * FROM tnews', array());
        $collectionNews = array();

        if($queryResults !== false){
            foreach ($queryResults as $row) {
                $news = NewsFabrique::getNews($dataError,$row['id'],$row['auteur'],$row['text']);
                $collectionNews [] = $news ;
            }
        }
        else{
            $dataError['persistance'] = "Probleme d'accès aux données";
        }

        return $collectionNews;
    }

    public static function postNews(&$dataError, $id, $auteur, $text){
        $news = NewsFabrique::getNews($$dataError,$id,$auteur,$text);
        if(empty($dataError)){
            $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('UPDATE tnews SET auteur=?, text=? WHERE id=?',array($news->getAuteur(),$news->getText(),$news->getId()));

            if($queryResults === false){
                $dataError["persistance"] = "Probleme d'acces aux données .";
            }
        }

        return $news;
    }

    public  static function putNews(&$dataError, $auteur, $text){
        $news = NewsFabrique::getNews($dataError,"0000000000",$auteur,$text);
        if(empty($dataError)){
            $queryResults = false;
            $count = 0;

            while($queryResults === false && $count<= 3){
                $news->setId(Config::generateRandomId());
                $count++;
                $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('INSERT INTO tnews(id,auteur,text) VALUES (?,?,?)',array($news->getId(),$news->getAuteur(),$news->getText()));

            }
            if($queryResults === false){
                $dataError["persistance"] = "Probleme d'execution de la requete";
            }
        }
        return $news;
    }

    public static function deleteNews(&$dataError, $id){
        $news = self::getNewsById($dataError,$id);

        if(empty($dataError)){
            $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('DELETE FROM tnews WHERE id = ?',array($id));
            if($queryResults === false){
                $dataError["persistance"] =  "Probleme d'execution de la requete.";
            }
        }

        return $news;
    }

}

?>