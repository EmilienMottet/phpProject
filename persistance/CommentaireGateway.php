<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 17/12/2015
 * Time: 11:24
 */
class CommentaireGateway
{
    public static  function getCommentaireById(&$dataError, $id){
        $Commentaire="";
        if(isset($id)){
            $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('SELECT * FROM commentaires WHERE id = ?',array($id));
            if(isset($queryResults) && is_array($queryResults)){
              if(count($queryResults) === 1){
                  $row = $queryResults[0];
                  $Commentaire = CommentaireFabrique::getCommentaire($dataError,$row['id'],$row['auteur'],$row['text']);
              }else{
                  $dataError['persistance'] = "Commentaire introuvable";
              }
            }else{
                $dataError['persistance'] = "Impossible d'accéder aux données.";
            }
        }
        return $Commentaire;
    }

    public  static  function getCommentaireAllByNews(&$dataError, $newsId){
        $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('SELECT * FROM commentaires WHERE news = ?', array($newsId));
        $collectionCommentaire = array();
        if($queryResults === false){
            echo  "<br/>taile de queryresult : ".count($queryResults)."<br/>";
        }

        if($queryResults !== false){
            foreach ($queryResults as $row) {
                $Commentaire = CommentaireFabrique::getCommentaire($dataError,$row['id'],$row['auteur'],$row['text']);
                $collectionCommentaire [] = $Commentaire ;
            }
        }
        else{
            $dataError['persistance'] = "Probleme d'accès aux données";
        }
        return $collectionCommentaire;
    }


    public  static  function getCommentaireAll(&$dataError){
        $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('SELECT * FROM commentaires', array());
        $collectionCommentaire = array();

        if($queryResults !== false){
            foreach ($queryResults as $row) {
                $Commentaire = CommentaireFabrique::getCommentaire($dataError,$row['id'],$row['auteur'],$row['text']);
                $collectionCommentaire [] = $Commentaire ;
            }
        }
        else{
            $dataError['persistance'] = "Probleme d'accès aux données";
        }

        return $collectionCommentaire;
    }

    public static function postCommentaire(&$dataError, $id, $auteur, $text){
        $Commentaire = CommentaireFabrique::getCommentaire($$dataError,$id,$auteur,$text);
        if(empty($dataError)){
            $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('UPDATE commentaires SET auteur=?, text=? WHERE id=?',array($Commentaire->getAuteur(),$Commentaire->getText(),$Commentaire->getId()));

            if($queryResults === false){
                $dataError["persistance"] = "Probleme d'acces aux données .";
            }
        }

        return $Commentaire;
    }

    public  static function putCommentaire(&$dataError, $auteur, $text, $newsId){
        $Commentaire = CommentaireFabrique::getCommentaire($dataError,"0000000000",$auteur,$text);
        if(empty($dataError)){
            $queryResults = false;
            $count = 0;

            while($queryResults === false && $count<= 3){
                $Commentaire->setId(Config::generateRandomId());
                $count++;
                $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('INSERT INTO commentaires(id,news,auteur,text) VALUES (?,?,?,?)',array($Commentaire->getId(),$newsId,$Commentaire->getAuteur(),$Commentaire->getText()));

            }
            if($queryResults === false){
                $dataError["persistance"] = "Probleme d'execution de la requete";
            }
        }
        return $Commentaire;
    }

    public static function deleteCommentaire(&$dataError, $id){
        $Commentaire = self::getCommentaireById($dataError,$id);

        if(empty($dataError)){
            $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('DELETE FROM commentaires WHERE id = ?',array($id));
            if($queryResults === false){
                $dataError["persistance"] =  "Probleme d'execution de la requete.";
            }
        }

        return $Commentaire;
    }

}

?>