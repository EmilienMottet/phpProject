<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 14/12/2015
 * Time: 07:34
 */
class DataBaseManager
{
    static  private $dbh = null;

    static private $instance = null;

    static private $db_host = "mysql:host=localhost;";
    static private $db_name = "dbname=projet";
    static private $db_user="root";
    static private $db_password="";

    /*
     * s t a t i c privat e $db_host=”mysql :host=l o c a l h o s t ; ” ;
23 s t a t i c privat e $db_name=”dbname=ExampleDataBase” ;
24 s t a t i c privat e $db_user=”remy” ;
25 s t a t i c privat e $db_password=”my_password” ;
     *
     */

    private function __construct()
    {
        try{
            self::$dbh = new PDO(self::$db_host.self::$db_name, self::$db_user,self::$db_password);
            //self::$dbh = new PDO('mysql:host=localhost;dbname=projet', 'root', '');
            self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            throw new Exception("Erreur de connexion à la base de données.");
        }
    }

    public static function getInstance()
    {
        if(null === self::$instance){
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function prepareAndExecuteQuery($requete, $args){
        $numargs = count($args);

        if(empty($requete) || !is_string($requete) || preg_match('/(\"|\')+/', $requete) !== 0){
            throw new Exception("Erreur concernant la sécurité");
        }

        try{
            $statement = self::$dbh->prepare($requete);
            if($statement !== false ){
                for ($i =1 ; $i <= $numargs; $i++){
                    $statement->bindParam($i, $args[$i-1]);
                }
                $statement->execute();
            }
        }catch(Exception $e){
            return false;
        }

        try{
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);
            $statement->closeCursor();
        }catch(PDOException $e){
            $results = true;
        }

        return $results;
    }

    private function __clone() {}
}