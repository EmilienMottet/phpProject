<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 22/12/2015
 * Time: 00:13
 */
class UserGateway
{
    public static function getRoleByPassword(&$dataError, $email, $hashedPassword){
        $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('SELECT * FROM User WHERE email = ?', array($email));

        if($queryResults !== false){
            if(count($queryResults) == 1){
                $row = $queryResults[0];
            }
            if(count($queryResults) != 1 || $row['password'] != $hashedPassword){
                $dataError['login'] = 'Adresse email ou mot de passe incorrect';
                return false;
            }
            return $row['role'];
        }
        else{
            $dataError['login'] = "Impossible d'acceder à la table d'utilisateurs";
            return false;
        }
    }


    public static function putUser(&$dataError, $email, $hashedPasswd){
        // on ne créer pas de nouvelle classe car on a que un mail et un pwd
        if(empty($dataError)){
            $queryResults = false;
            $queryResults = DataBaseManager::getInstance()->prepareAndExecuteQuery('INSERT INTO User(email,password,role) VALUES (?,?,?)',array($email,$hashedPasswd,"users"));
            if($queryResults === false){
                $dataError["persistance"] = "Probleme d'execution de la requete";
                return false;
            }
            else{
                return "users";
            }
        }
        return false;
    }

}