<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 13/12/2015
 * Time: 23:45
 */
class CommentaireFabrique
{
    public static function getCommentaire(&$dataErrors,$id , $auteur , $text, $date){
        $Commentaire = Commentaire::getDefaultCommentaire();

        $dataErrors = array();

        try{
            $Commentaire->setId($id);
        }catch(Exception $e){
            $dataErrors['id'] = $e->getMessage()."<br/>\n";
        }

        try{
            $Commentaire-> setAuteur($auteur);
        }catch (Exception $e){
            $dataErrors['auteur'] = $e->getMessage()."<br/>\n";
        }
        try{
            $Commentaire -> setText($text);
        }catch (Exception $e){
            $dataErrors['text'] = $e->getMessage()."<br/>\n";
        }
        try{
            $Commentaire-> setDate($date);
        }catch (Exception $e){
            $dataErrors['date'] = $e->getMessage()."<br/>\n";
        }

        return $Commentaire;
    }
}

?>