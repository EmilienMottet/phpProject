<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 21/12/2015
 * Time: 23:46
 */
class Authentication
{
    public static function checkAndInitiateSession($login,$password, $dataError){
        if(!empty($dataError)){
            return new Model($dataError);
        }

        $userModel = ModelUser::getModelUser($login,hash("sha512",$password));
        if($userModel->getError() !== false){
            return $userModel;
        }

        $mySid = AuthUtils::generateSessionId();
        session_id($mySid);

        setcookie("session-id",$mySid,time()+60*2);
        session_start();
        session_destroy();

        session_cache_expire(2);
        session_id($mySid);

        session_start();

        $userModel->fillSessionData();
        session_write_close();
        return $userModel;
    }

    public static function restoreSession(){
        $dataError = array();

        if(!isset($_COOKIE['session-id']) || !preg_match("/^[0-9a-fA-F]{20}$/",$_COOKIE['session-id'])){
            $dataError['no-cookie'] = "Votre cookie a peut etre expirée, Merci de vous connecter à nouveau ...";
            $userModel = new Model($dataError);
        }
        else{
            $mySid = $_COOKIE['session-id'];
            session_id($mySid);
            session_start();
            setcookie("session-id",$mySid,time()+60*2);
            $userModel= ModelUser::getModelUserFromSession();
            session_write_close();
        }

        return $userModel;
    }

    public static function nouvelUser($login,$password, $dataError){
        if(!empty($dataError)){
            return new Model($dataError);
        }

        $userModel = ModelUser::getModelUserPut($login,hash("sha512",$password));

        if($userModel->getError() !== false){
            return $userModel;
        }

        return Authentication::checkAndInitiateSession($login,$password ,$dataError);

    }
}

?>