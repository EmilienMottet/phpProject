<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 13/12/2015
 * Time: 13:41
 */

//require_once('./CommentaireProperties.php'); useless il suffit d'uitliser le use apperrement

class Commentaire
{
    use NewsProperties;

    private $id;

    private $auteur;

    private $text;

    private $date;

    private static function generateRandomId(){
        $cryptoStrong = false;
        $octet = openssl_random_pseudo_bytes(5, $cryptoStrong);
        return bin2hex($octet);
    }

    public function __construct($id, $auteur, $text, $date)
    {
        $this->setAuteur($auteur);
        $this->setText($text);
        $this->setDate($date);
    }

    public static function getDefaultCommentaire(){
        $Commentaire = new Commentaire(12,"John","Doe");
        $Commentaire->id = self::generateRandomId();
        $Commentaire->auteur = "";
        $Commentaire->text = "";
        $Commentaire->date="";
        return $Commentaire;
    }

}

?>