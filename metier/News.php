<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 13/12/2015
 * Time: 13:41
 */

//require_once('./NewsProperties.php'); useless il suffit d'uitliser le use apperrement

class News
{
    use NewsProperties;

    private $id;

    private $auteur;

    private $text;

    private $date;

    private $commentaires = array();

    public function getCommentaires()
    {
        return $this->commentaires;
    }

    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    private static function generateRandomId(){
        $cryptoStrong = false;
        $octet = openssl_random_pseudo_bytes(5, $cryptoStrong);
        return bin2hex($octet);
    }

    public function __construct($id, $auteur, $text, $date)
    {
        $this->setAuteur($auteur);
        $this->setText($text);
        $this->setDate($date);
    }

    public static function getDefaultNews(){
        $News = new News(12,"John","Doe");
        $News->id = self::generateRandomId();
        $News->auteur = "";
        $News->text = "";
        $News->date="";
        return $News;
    }

}

?>