<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 13/12/2015
 * Time: 14:18
 */
trait NewsProperties
{
    public function getId(){
        return $this->id;
    }

    public function getAuteur(){
        return $this->auteur;
    }

    public function getText(){
        return $this->text;
    }

    public function getDate(){
        return $this->date;
    }

    public function setAuteur($auteur){
        $regex_FR_LANG = ExpressionsRegexUtils::getRegexFrLang() ;
        // le pb est que le regex est vide
        if(!ExpressionsRegexUtils::isValidString($auteur, $regex_FR_LANG, 1 , 150)){
            throw new Exception("Erreur le nom de l'auteur, doit comporter au plus 150 caractères");
        }
        $this->auteur = empty($auteur) ? "": $auteur;
    }

    public function setText($text){
        $regex_FR_LANG_WITH_NUMBERS = ExpressionsRegexUtils::getRegexFrLangWithNumbers();
        if(!ExpressionsRegexUtils::isValidString($text,$regex_FR_LANG_WITH_NUMBERS,1,150)){
            throw new Exception("Erreur le message ne peut etre vide et doit comporter uniquement des caractere alphanumérique");
        }
        $this->text = empty($text) ? "": $text;
    }

    public function setId($id){
        if(!isset($id) || !preg_match("/^[0-9a-f]{1,10}$/", $id)){
            throw new Exception("Erreur identifiant incorrect");
        }
        $this->id=$id;
    }

    public function  setDate($date){
        $regex_FR_LANG_WITH_NUMBERS = ExpressionsRegexUtils::getRegexFrLangWithNumbers();
        if(!ExpressionsRegexUtils::isValidString($date,$regex_FR_LANG_WITH_NUMBERS,1,150)){
            throw  new Exception("Erreur la date n'est pas correcte");
        }
        $this->date=$date;
    }


}