<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 13/12/2015
 * Time: 23:45
 */
class NewsFabrique
{
    public static function getNews(&$dataErrors,$id , $auteur , $text, $date){
        $news = News::getDefaultNews();

        $dataErrors = array();

        try{
            $news->setId($id);
        }catch(Exception $e){
            $dataErrors['id'] = $e->getMessage()."<br/>\n";
        }

        try{
            $news-> setAuteur($auteur);
        }catch (Exception $e){
            $dataErrors['auteur'] = $e->getMessage()."<br/>\n";
        }
        try{
            $news -> setText($text);
        }catch (Exception $e){
            $dataErrors['text'] = $e->getMessage()."<br/>\n";
        }
        try{
            $news-> setDate($date);
        }catch (Exception $e){
            $dataErrors['date']= $e->getMessage()."<br/>\n";
        }

        return $news;
    }

    public static function getNewsAvecCommentaires(&$dataErrors,$id , $auteur , $text, $commentaires, $date){

        $news = NewsFabrique::getNews($dataErrors,$id,$auteur,$text, $date);

        try{
            if(isset($commentaires)){
                $news -> setCommentaires($commentaires);
            }
        }catch(Exception $e){
            $dataErrors['commentaires'] = $e->getMessage()."<br/>\n";
        }
        return $news;
    }
}

?>