<?php
/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 22/12/2015
 * Time: 14:47
 */

//BIZARRE qu'on utilise que $dataError['login'] ... peut etre en mettre un password ou email


$dataError = array();

$wouldBePasswd = isset($_POST['motdepasse']) ? $_POST['motdepasse'] : "";
if( $wouldBePasswd == "" || !AuthUtils::isStrongPassword($wouldBePasswd)){
    $password = "";
    $dataError["login"] = "<p>Mot de passe incorrect votre mot de passe doit contenir au moins 8 caracteres, une minuscule, une majuscule et un chiffre </p>";
}
else{
    $password = $wouldBePasswd;
}

if (!isset($_POST["email"]) || filter_var($_POST["email"],FILTER_VALIDATE_EMAIL) === false){
    $email = "";
    $dataError["login"] = "Adresse e-mail invalide";
}
else{
    $email = $_POST['email'];
}


?>