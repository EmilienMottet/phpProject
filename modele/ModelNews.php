<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 16/12/2015
 * Time: 16:30
 */
class ModelNews extends Model
{
    private $news;

    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function getData(){
        return $this->news;
    }

    public static function getModelDefaultNews(){
        $model = new self(array());
        $model->news = News::getDefaultNews();
        $model->title = "Saisie d'une news";
        return $model;
    }

    public static function getModelNews($id){
        $model = new self(array());
        $model->news = NewsGateway::getNewsById($model->dataError, $id);
        $model->title = "Affichage d'une news";
        return $model;
    }

    public static function getModelNewsPost($id, $auteur, $text, $date){
        $model = new self(array());
        $model->news = NewsGateway::postNews($model->dataError,$id,$auteur,$text,$date);
        $model->title = "la news a été mis à jour";

        return $model;
    }

    public static function getModelNewsPut($auteur,$text){
        $model = new self(array());
        $model->news = NewsGateway::putNews($model->dataError, $auteur, $text);
        $model->title = "La news a été inséré";
        return $model;
    }

    public static  function  deleteNews($id){
        $model = new self(array());
        $model->news = NewsGateway::deleteNews($model->dataError,$id);
        $model->title = "News supprimé";
        return $model;
    }

}

?>