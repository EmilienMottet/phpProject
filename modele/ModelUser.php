<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 21/12/2015
 * Time: 23:49
 */
class ModelUser extends Model
{
    private $email;

//    private $pseudo; potentiellement faire une classe pour avoir + d'info sur les users

    private $role;

    public function getRole()
    {
        return $this->role;
    }

    public static function getModelUser($email,$hashedPassword){
//        echo "<br/> voici le pass hashé ".$hashedPassword."<br/>";  // 3e908a3c1b805bc28e2ca399e5ac43d9b16bf91e4cde4241f39d732e8224383853018e31097e5fe4e2dae736fda54754be5fead95832f3a83ede11287c76ca27
        $model = new self(array());
        $model->role = UserGateway::getRoleByPassword($model->dataError, $email, $hashedPassword);
        if($model->role !== false ){
            $model->email = $email;
        }
        else{
            $model->dataError['login'] = "Login ou mot de passe incorrect";
        }
        return $model;
    }

    public function fillSessionData(){
        $_SESSION['email'] = $this->email;
        $_SESSION['role'] = $this->role;
        $_SESSION['ipAdress'] = $_SERVER['REMOTE_ADDR'];
    }

    public static function getModelUserFromSession(){
        $model = new self(array());
        if(!isset($_SESSION['email']) || !isset($_SESSION['role']) || !isset($_SESSION['ipAdress']) || ($_SESSION['ipAdress'] != $_SERVER['REMOTE_ADDR'])){
            $model->dataError{'session'} = "Unable to recover user session";
        }
        else{
            $model->role = $_SESSION['role'];
            $model->email = $_SESSION['email'];
        }

        return $model;

    }

    public static function getModelUserPut($email,$hashedPassword){
        $model = new self(array());
        $model->role = UserGateway::putUser($model->dataError, $email, $hashedPassword);
        if($model->role !== false ){
            $model->email = $email;
        }
        else{
            $model->dataError['login'] = "Inscription impossible (peut etre email deja utilisé )";
        }
        return $model;
    }

}