<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 20/12/2015
 * Time: 15:36
 */
class ModelCollectionNews extends Model
{
    private $collectionNews;

    public function getData(){
        return $this->collectionNews;
    }

    public static function  getModelNewsAll(){
        $model = new self(array());
        $model->collectionNews = NewsGateway::getNewsAll($model->dataError);
        return $model;
    }



}