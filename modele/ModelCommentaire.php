<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 16/12/2015
 * Time: 16:30
 */
class ModelCommentaire extends Model
{
    private $Commentaire;

    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function getData(){
        return $this->Commentaire;
    }

    public static function getModelDefaultCommentaire(){
        $model = new self(array());
        $model->Commentaire = Commentaire::getDefaultCommentaire();
        $model->title = "Saisie d'une Commentaire";
        return $model;
    }

    public static function getModelCommentaire($id){
        $model = new self(array());
        $model->Commentaire = CommentaireGateway::getCommentaireById($model->dataError, $id);
        $model->title = "Affichage d'une Commentaire";
        return $model;
    }

    public static function getModelCommentairePost($id, $auteur, $text){
        $model = new self(array());
        $model->Commentaire = CommentaireGateway::postCommentaire($model->dataError,$id,$auteur,$text);
        $model->title = "la Commentaire a été mis à jour";

        return $model;
    }

    public static function getModelCommentairePut($auteur,$text,$newsId){
        $model = new self(array());
        $model->Commentaire = CommentaireGateway::putCommentaire($model->dataError, $auteur, $text,$newsId);
        $model->title = "Le commentaire a été inséré";
        return $model;
    }

    public static  function  deleteCommentaire($id){
        $model = new self(array());
        $model->Commentaire = CommentaireGateway::deleteCommentaire($model->dataError,$id);
        $model->title = "Commentaire supprimé";
        return $model;
    }

}

?>