<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 13/12/2015
 * Time: 13:28
 */
class Model
{
    protected $dataError;

    public function getError()
    {
        if(empty($this->dataError)){
            return false;
        }
        return $this->dataError;
    }

    public function __construct($dataError)
    {
        $this->dataError = $dataError;
    }
}

?>