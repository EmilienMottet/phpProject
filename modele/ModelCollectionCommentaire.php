<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 20/12/2015
 * Time: 15:36
 */
class ModelCollectionCommentaire extends Model
{
    private $collectionCommentaire;

    public function getData(){
        return $this->collectionCommentaire;
    }

    public static function  getModelCommentaireAll(){
        $model = new self(array());
        $model->collectionCommentaire = CommentaireGateway::getCommentaireAll($model->dataError);
        return $model;
    }

    public static function getModelCommentaireAllByNews($news){
        $model = new self(array());
        $model->collectionCommentaire = CommentaireGateway::getCommentaireAllByNews($model->dataError,$news->getId());
        return $model;
    }



}