<?php

class Config
{
    public static function getVues(){
    global  $rootDirectory;
    $vueDirectory = $rootDirectory."vue/";
    return array("default"=>$vueDirectory."vueAccueil.php",
                 "defaultAdmin"=>$vueDirectory."vueAccueil.php",
                 "defaultUser"=>$vueDirectory."vueAccueil.php",
                 "afficheCollectionNewsAdmin"=>$vueDirectory."vueCollectionNewsAdmin.php",
                 "saisieNews"=>$vueDirectory."vueSaisieNews.php",
                 "editionNews"=>$vueDirectory."vueEditionNews.php",
                 "afficheNewsVisiteur"=>$vueDirectory."vueAfficheNewsVisiteur.php",
                 "afficheNews"=>$vueDirectory."vueAfficheNews.php",
                     "afficheCollectionNews"=>$vueDirectory."vueCollectionNews.php",
                 "inscription"=>$vueDirectory."vueInscription.php",
                 "authentification"=>$vueDirectory."vueAuthentification.php");
    }
    
    public static function getVuesError(){
        global $rootDirectory;
        $vueDirectory = $rootDirectory."vue/";
        return array("default"=>$vueDirectory."vueErreurDefault.php",
                        "saisieNewsError"=>$vueDirectory."vueSaisieNewsError.php");
    }
    
    public static function getStyleSheetsURL(){
        $cssDirectoryURL = "http://".$_SERVER['HTTP_HOST']."/example/mvc/css";
        return array("default"=>$cssDirectoryURL."defaultStyle.css");
    }
    
    public static function generateRandomId(){
        $cryptoStrong=false;
        $octets=  openssl_random_pseudo_bytes(5, $cryptoStrong);
        return bin2hex($octets);
    }
    
    public static function generateSessionId(){
        return generateRandomId().hash("md5",$_SERVER['REMOVE_ADDR']);
    }

    public  static function getRootURI(){
        global $rootURI;
        return $rootURI;
    }
}
?>

