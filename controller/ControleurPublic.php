<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 22/12/2015
 * Time: 15:12
 */
class ControleurPublic
{
    function __construct($action)
    {
        echo "controlleur public";
        try{
            $validation = dirname(__FILE__)."/../metier/validationNews.php";
            switch($action){
                case "inscription" :
                    $model = new Model(array());
                    require (Config::getVues()['inscription']);
                    break;
                case "auth" :
                    $model = new Model(array());
                    require (Config::getVues()["authentification"]);
                    break;
                case "validateInsc" :
                    $email ="";
                    $password ="";
                    $dataError="";
                    require (dirname(__FILE__)."/../metier/validationInscription.php");
                    $model = Authentication::nouvelUser($email, $password, $dataError);
                    if($model->getError() === false){
                        if($model->getRole() == "admin"){
                            require (Config::getVues()["defaultAdmin"]);
                        }
                        else{
                            require (Config::getVues()["defaultUser"]);
                        }
                    }
                    else{
                        require (Config::getVues()["inscription"]);
                    }
                    break;

                case "validateAuth" :
                    $email ="";
                    $password ="";
                    $dataError="";
                    require (dirname(__FILE__)."/../metier/validationLogin.php");
                    $model = Authentication::checkAndInitiateSession($email, $password, $dataError);
                    if($model->getError() === false){
                        if($model->getRole() == "admin"){
                            require (Config::getVues()["defaultAdmin"]);
                        }
                        else{
                            require (Config::getVues()["defaultUser"]);
                        }
                    }
                    else{
                        require (Config::getVues()["authentification"]);
                    }
                    break;
                case "afficheNews" :
                    $rawId = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
                    $id = filter_var($rawId,FILTER_SANITIZE_STRING);
                    $model = ModelNews::getModelNews($id);
                    if($model ->getError() === false ){
                        require (Config::getVues()["afficheNewsVisiteur"]);
                    }
                    else{
                        require (Config::getVuesError()["default"]);
                    }
                    break;
                case "afficheCollectionNews" :
                    $model = ModelCollectionNews::getModelNewsAll();
                    if($model->getError() === false){
                        require (Config::getVues()["afficheCollectionNews"]);
                    }
                    else{
                        require (Config::getVuesError()["default"]);
                    }
                    break;
                default :
                    require (Config::getVues()["default"]);
                    break;

            }
        }catch(Exception $e){
            return Config::getVuesError()['default'];
        }
    }
}

?>