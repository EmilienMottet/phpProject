<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 22/12/2015
 * Time: 15:11
 */
class ControleurAdmin
{
    function __construct($action)
    {
        echo "controlleur admin  ".$action;
        try{
            $validation = dirname(__FILE__)."/../metier/validationNews.php"; // peut plus nécessaire
            switch($action){
                case "putCommentaire" :
                    $auteur = "";
                    $text ="";
                    $rawnewsId = isset($_REQUEST['newsId']) ? $_REQUEST['newsId'] : "";
                    $newsId = filter_var($rawnewsId,FILTER_SANITIZE_STRING);
                    echo $newsId;
                    require (dirname(__FILE__)."/../metier/validationNews.php");
                    $model = ModelCommentaire::getModelCommentairePut($auteur,$text,$newsId);
                    if($model->getError() === false){
                        $model = ModelNews::getModelNews($newsId);
                        require(Config::getVues()["afficheNews"]);
                    }
                    else{
                        if(!empty($model->getError()['persistance'])){
                            //echo "voici le message d'erreur : ".$model->getError()['persistance']."<br/>";
                                require (Config::getVuesError()['default']);
                        }
                        else{
                            require (Config::getVuesError()['saisieNewsError']);
                        }
                    }
                    break;
                case "editionNews" :
                    require(Config::getVues()["editionNews"]);
                    break;
                case "afficheNews" :
                    $rawId = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
                    $id = filter_var($rawId,FILTER_SANITIZE_STRING);
                    $model = ModelNews::getModelNews($id);
                    if($model ->getError() === false ){
                        require (Config::getVues()["afficheNews"]);
                    }
                    else{
                        require (Config::getVuesError()["default"]);
                    }
                    break;
                case "afficheCollectionNews" :
                    $model = ModelCollectionNews::getModelNewsAll();
                    if($model->getError() === false){
                        require (Config::getVues()["afficheCollectionNewsAdmin"]);
                    }
                    else{
                        require (Config::getVuesError()["default"]);
                    }
                    break;
                case "saisieNews" :
                    require(Config::getVues()["saisieNews"]);
                    break;
                case "put" :
                    $auteur = "";
                    $text ="";
                    require (dirname(__FILE__)."/../metier/validationNews.php");
                    $model = ModelNews::getModelNewsPut($auteur,$text);
                    if($model->getError() === false){
                        require(Config::getVues()["afficheNews"]);
                    }
                    else{
                        if(!empty($model->getError()['persistance'])){
                            //echo "voici le message d'erreur : ".$model->getError()['persistance']."<br/>";
                            require (Config::getVuesError()['default']);
                        }
                        else{
                            require (Config::getVuesError()['saisieNewsError']);
                        }
                    }

                    break;
                case "delete" :
                    $rawId = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
                    $id = filter_var($rawId,FILTER_SANITIZE_STRING);
                    $model = ModelNews::deleteNews($id);
                    if($model->getError() === false){
                        require (Config::getVues()['afficheNewsVisiteur']);
                    }
                    else{
                        require (Config::getVuesError()['default']);
                    }
                    break;
                case "edit" :
                    $rawId = isset($_REQUEST['id']) ?  $_REQUEST['id'] : "";
                    $id = filter_var($rawId, FILTER_SANITIZE_STRING);
                    $model = ModelNews::getModelNews($id);
                    if($model->getError() === false){
                        require (Config::getVues()['editionNews']);
                    }
                    else{
                        require (Config::getVuesError()['default']);
                    }
                    break;
                default:
                    require(Config::getVues()["default"]);
            }
        }catch(Exception $e){
            $model = new Model(array('exception' => $e->getMessage()));
            require (Config::getVuesError()['default']);
        }
    }
}