<?php

/**
 * Created by PhpStorm.
 * User: emimo
 * Date: 21/12/2015
 * Time: 23:37
 */

/*
 * idée d'amélio : amélio les news ( genre mettre une photo dans une news
 * mettre l'édition ( de news voir de comm )
 * ajout des dates au com et au news
 * ajout d'une fonction déconnexion
 *
 *
 */

class ControleurFront
{

    function __construct()
    {
        echo "frontController";
        try{
            $validation = dirname(__FILE__)."/../metier/validationNews.php";
            $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "";

            $model = Authentication::restoreSession();

            $role = ($model->getError() === false ) ? $model->getRole() : "";

            switch($action){
                case "inscription" :
                case "auth" : // vue de saise login/password  //bidouille pour ne pas avoir la vu de connexion quand on est connecter
                case "validateInsc" :
                case "validateAuth" :
                    if($role != "admin" && $role != "users"){
                        $publicCtrl = new ControleurPublic($action);
                    }
                    else{
                        if($role == "users"){
                            // a completer
                            $userCtrl = new ControleurUser($action);
                        }
                        if($role == "admin"){
                            $adminCtrl = new ControleurAdmin($action);
                        }
                    }
                    break;
                case "saisieNews" :
                case "post" : // pas encore implementer
                case "put" :
                case "delete" :
                    if($role == "admin"){
                        $adminCtrl = new ControleurAdmin($action);
                    }
                    else
                    {
                        require (Config::getVues()["authentification"]);
                    }
                    break;
                case "putCommentaire" :
                    if( $role == "admin"){
                        $adminCtrl = new ControleurAdmin($action);
                    }else{
                        if($role == "users"){
                            $userCtrl = new ControleurUser($action);
                        }else{
                            require (Config::getVues()["authentification"]);
                        }
                    }
                    break;
                case "afficheNews" :
                case "afficheCollectionNews" :
                default :
                    if($role == "admin"){
                        $adminCtrl = new ControleurAdmin($action);
                    }
                    else{
                        if($role == "users"){
                            $userCtrl= new ControleurUser($action);
                        }
                        else {
                            $publicCtrl = new ControleurPublic($action);
                        }
                    }
                    break;
            }

        }catch(Exception $e){
            $model = new Model(array('exception' => $e->getMessage()));
            require (Config::getVuesError()['default']);
        }
   }

}